from recon.core.module import BaseModule

class Module(BaseModule):

    meta = {
        'name': 'Bing-Cache Linkedin Profile and Contact Harvester',
        'author':'Joe Black and @fullmetalcache',
        'description': 'Harvests profiles from linkedin.com by querying the Bing API Cache for Linkedin pages related to the given companies, and adding them to the \'profiles\' table. It will then parse the result information to extract the user\'s Full Name and Title (title parsing is a bit spotty currently). The user\'s Full Name and Title are then added to the \'contacts\' table. This module does not access Linkedin at any time. It will only query Bing\'s API which seems to give better and faster results. Be sure to set the Subdomains option to the region your orginization is located in.  You will get better results if you use more subdomains other than just \'www\'.',
        'query': 'SELECT DISTINCT company FROM companies WHERE company IS NOT NULL',
        'options': (
            ('limit', 0, True, 'number of pages to use from bing search (0 = unlimited)'),
            ('subdomains', False, True, 'Change the subdomain search for Linkedin such as www, ca, uk...You can use a comma seperated list to run the search on multiple subdomains'),
        ),
    }

    def module_run(self, companies):
        for company in companies:
            self.heading(company, level=0)
            self.get_profiles(company)
            if " " in company:
                company = company.replace(" ", "")
                self.get_profiles(company)

    def get_contact_info(self, cache):
            cache_splt = cache.split('Description:')
            title = cache_splt[0].split('Title:')[1]
            description = cache_splt[1]
            fullname, fname, mname, lname = self.parse_fullname(title)
            jobtitle = self.parse_jobtitle(fullname, description)
            self.alert('%s %s - %s' % (fname, lname, jobtitle))
            self.add_contacts(first_name=fname, middle_name=mname, last_name=lname, title=jobtitle)

    def get_profiles(self, company):
        results = []

        subdomain = self.options['subdomains']
        if subdomain == False:
            subdomain_list = ['']
        else:
            subdomain_list = subdomain.split(',')

        for sub in subdomain_list:
            sub = sub.strip()
            base_query = ["site:\"%s.linkedin.com/in/\" && %s"  % (sub, company),
            "site:%s.linkedin.com -jobs && %s" % (sub, company),
            "site:%s.linkedin.com instreamset:(url):\"pub\" -instreamset:(url):\"dir\" && %s" % (sub, company)]

            for query in base_query:

                retries = 5

                while retries > 0:
                    try:
                        results = self.search_bing_api(query, self.options['limit'])
                        break
                    except:
                        retries -= 1
                        self.error('Error fetching results, %s retries left' % retries)
                        continue

                for result in results:
                    url = result['Url']
                    description = result['Description']
                    title = result['Title']
                    cache = 'Title:%sDescription:%s' % (title, description)

                    if '/pub/dir/' not in url and company.lower() not in title.lower():
                        if company.lower() in description.lower():
                            username = self.parse_username(url) or 'unknown'
                            self.verbose('Parsing \'%s\'...' % (url))
                            self.alert('Probable match: %s' % url)
                            self.add_profiles(username=username, url=url, resource='bing_linkedin_cache', category='social')

                            self.get_contact_info(cache)

    def parse_fullname(self, title):
        fullname = title.split(" |")[0]
        fullname = fullname.split(",")[0]
        names = self.parse_name(fullname)
        fname = names[0]
        mname = names[1]
        lname = names[2]
        return fullname, fname, mname, lname

    def parse_jobtitle(self, fullname, description):
        jobtitle = 'Employee'

        titles = description.split('at')
        if len(titles) > 1:
            jobtitle = titles[0]
            try:
                jobtitle = jobtitle.split(fullname)[1]
                jobtitle = jobtitle.replace(', ', '', 1)
                jobtitle = jobtitle.replace('. ', '', 1)
            except IndexError:
                jobtitle = 'Employee'

        return jobtitle

    def parse_username(self, url):
        username = None
        if '/in/' in url:
            url = url.split('/in/')[1]
            username = url.rsplit('-',1)[0]
        elif '/pub/' in url:
            url = url.split('/pub/')[1]
            username = url.split('/')[0]
        return username
